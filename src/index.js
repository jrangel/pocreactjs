import React from 'react';
import ReactDOM from 'react-dom';
import Questions from './components/questions/index.jsx';

let items = [
  { id: 0, title: 'test', body: 'work!', showButton: true}
  ,{ id: 1, title: 'hola', body: 'work!', showButton: true}
  ,{ id: 2, title: 'hola 2', body: 'work!', showButton: true}
  ,{ id: 3, title: 'titulo', body: 'work!', showButton: true}
  ,{ id: 4, title: 'prueba', body: 'work!', showButton: true}
  ,{ id: 5, title: 'prueba 1', body: 'work!', showButton: true}
];

ReactDOM.render(
  <Questions items={items}></Questions>,
  document.getElementById('root')
);
