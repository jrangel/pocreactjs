import React from 'react'
import Questions from '../components/questions/index.jsx'

function renderQuestions(container, data, addQuestion, deleteQuestion, callback){
  console.log('renderQuestions: ', container, data, addQuestion, deleteQuestion, callback);
  ReactDOM.render(<Questions items={data} afterAddQuestion={addQuestion}
      afterDeleteQuestion={deleteQuestion}></Questions>, document.getElementById(container), callback);
}

module.exports = renderQuestions;
window.renderQuestions = renderQuestions;
