import React from 'react'
import Search from '../components/search/index.jsx'

function renderSearch(container, data, callback){
  ReactDOM.render(<Search items={data}></Search>, document.getElementById(container), callback);
}

module.exports = renderSearch;
window.renderSearch = renderSearch;
