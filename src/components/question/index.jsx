import React from 'react';

class Question extends React.Component {
  constructor(props) {
    super(props);
  }

  handleDelete(){
    this.props.onRemove(this.props.id);
  }

  render(){
    const showButton = (this.props.showButton == true)
      ? <button onClick={this.handleDelete.bind(this)}>Eliminar</button>
      : null;

    return (
      <li className={"list-group-item"}>{this.props.title} -
        <a > {this.props.body} </a>
        {showButton}
      </li >
    );
  }
};

module.exports = Question;
