import React from 'react';
import Question from '../question/index.jsx';
import './index.styl'

class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      questions: this.props.items,
      searchString: ''
    };
  }

  componentWillReceiveProps(nextProps){
    this.setState({ questions: nextProps.items.sort((a, b) => b.id > a.id) });
  }

  handleChange(e) {
    this.setState({ searchString: e.target.value });
  }

  handleDelete(id) {
    console.log(this);
    this.setState({ questions: this.state.questions.filter(q => q.id != id) });
    this.props.afterDeleteQuestion(id);
  }

  render() {

    let questions = this.state.questions,
      searchString = this.state.searchString.trim().toLowerCase();

    if (searchString.length > 0) {
      questions = questions.filter(function(q) {
        return q.title.toLowerCase().match(searchString) || q.body.toLowerCase().match(searchString);
      });
    }

    return (
      <div className={"panel panel-success"}>
        <div className={"panel-heading"}>
          Busqueda modificada
          <input className={"text-search"} type="text" value={this.state.searchString}
            onChange={this.handleChange.bind(this)} placeholder="buscar"/>
          filtrando {questions.length} preguntas de {this.state.questions.length}
       </div>
        <div className={"panel-body"}>
          <ul className={"list-group"}>
            {questions.map((q, i) =>
                <Question key={i} id={q.id} title={q.title}
                  body={q.body} showButton={q.showButton}
                  onRemove={this.handleDelete.bind(this)}>
                </Question>)}
          </ul>
        </div>
      </div>
      );
  }
};

Search.defaultProps = {
    items : [],
    afterDeleteQuestion: function(){}
}

module.exports = Search;
