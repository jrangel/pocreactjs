import React from 'react';
import QuestionForm from '../questionform/index.jsx';
import Search from '../search/index.jsx';

class Questions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.items,
      maxId: 99999999
    }
  }

  componentWillReceiveProps(nextProps){
    console.log('componentWillReceiveProps: ', nextProps);
    this.setState({ items: nextProps.items });
  }

  handleAddQuestion(item){
    console.log('handleAddQuestion: ', item);
    let _items = this.state.items;
    _items.push(item);
    this.setState({ items: _items, maxId: item.id + 1 });
    this.props.afterAddQuestion(item);
  }

  handleDelete(id){
    this.setState({ items: this.state.items.filter(q => q.id != id) });
  }

  render(){
    return (
      <div>
        <QuestionForm initialID={this.state.maxId} addQuestion={this.handleAddQuestion.bind(this)}></QuestionForm>
        <Search items={this.state.items} afterDeleteQuestion={this.handleDelete.bind(this)} ></Search>
      </div>
    );
  }
}

Questions.defaultProps = {
  items: [],
  afterAddQuestion: function() {},
  afterDeleteQuestion: function() {}
}

module.exports = Questions;
