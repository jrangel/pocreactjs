import React from 'react';
import './index.styl';

class QuestionForm extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      title: '',
      body: ''
    };
  }

  handleChangeTitle(event){
    this.setState({ title: event.target.value });
  }

  handleChangeBody(event){
    this.setState({ body: event.target.value });
  }

  handleSubmit(event){
    event.preventDefault();
    let data = {
      id: this.props.initialID,
      title: this.state.title,
      body: this.state.body,
      showButton: true
    };
    this.props.addQuestion(data);
    this.setState({ title: '', body: ''});
  }

  render(){
    let buttonSend = <input disabled className={"btn btn-primary "} type="submit" value="Enviar" />;
    if (this.state.title.length > 0 && this.state.body.length > 0)
      buttonSend = <input className={"btn btn-primary "} type="submit" value="Enviar" />;
    return (
      <div className={"questionform row"}>
      <form className={"form-inline"} onSubmit={this.handleSubmit.bind(this)}>
        <div className={"form-group"}>
          <label>Titulo</label>
          {' '}
          <input className={"form-control"} onChange={this.handleChangeTitle.bind(this)}
            id="questionTitle" name="questionTitle" placeholder="tema"
            value={this.state.title} />
        </div>
        <div className={"form-group"}>
          <label>Pregunta</label>
          {' '}
          <input className={"form-control"} onChange={this.handleChangeBody.bind(this)}
            id="questionBody" name="questionBody" placeholder="¿por que?"
            value={this.state.body} />
        </div>
        {buttonSend}
      </form>
      </div>
    );
  }
}

QuestionForm.defaultProps = {
  initialID: 99999999
}
module.exports = QuestionForm;
