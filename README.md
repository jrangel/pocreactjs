# PoC ReactJS #

Prueba de concepto para inflexion software sobre el uso de ReactJS

### Puntos importantes ###

* Uso de JSX como lenguaje
* Configuracion de webpack para exportar componentes

### Pre Requisitos ###

* Instalar [NodeJS](https://nodejs.org/es/)
* Instalar [Yarn](https://yarnpkg.com/en/docs/install) (no necesario pero recomendable)

### Pasos ###

1. Clonar repositorio

    ```git clone git@bitbucket.org:jrangel/pocreactjs.git```

2. Entrar en el directorio clonado

    ```cd /pocreactjs/```

3. Instalar dependencias

    ```yarn ó npm install (si no instalaste yarn)```

4. Compilar y arrancar el servidor

    ```npm start```

5. Listo!, ingresa al componente ReactJS

    ```http://localhost:3000/```
